# Programming Environment Change on Cori in March 2022

## Background

During the scheduled Cori maintenance on March 16, 2022, the operating system (OS) will be upgraded from CLE7.0UP01 to 
CLE7.0UP03, which includes upgrading the OS kernel from SLES15 to SLES15SP2.  We will install new CDT/22.02 and set it 
to default version (from CDT/19.11).  Intel compiler default will also be changesd to 19.1.2.254 (from 19.0.3.199). 
In addition, the old CDT/19.03, CDT/20.06, and CDT/20.10 will be removed.  

NERSC-supported software will be updated to be compatible with the new OS and new default CDT and Intel compiler versions. 
Users are highly recommended to rebuild applications as well. Notice statically compiled codes are required to relink.

Below is the detailed list of planned changes:

## Software default version changes

* atp/3.14.9 (from 2.1.3)
* cce/13.0.1 (from 9.1.0)
* cray-R/4.1.2.0 (from 3.6.1)
* cray-ccdb/4.12.8 (from 3.0.5)
* cray-cti/2.15.9 (from 1.0.9)
* cray-dyninst/12.0.0 (new)
* cray-fftw/3.3.8.10 (from 3.3.8.4)
* cray-hdf5,cray-hdf5-parallel/1.12.1.1  (from 1.10.5.2)
* cray-jemalloc/5.1.0.3 (from 5.1.0.1)
* cray-libsci/20.09.1 (from 19.06.1)
* cray-mpich,cray-mpich-abi,cary-shmem/7.7.19  (from 7.7.10)
* cray-netcdf,cray-netcdf-hdf5parallel/4.8.1.1  (from 4.6.3.2)
* cray-parallel-netcdf/1.12.2.1 (from 1.11.1.1)
* cray-petsc,cray-petsc-64,cray-petsc-complex,cray-petsc-complex-64/3.14.5.0  (from 3.11.2.0)
* cray-python/3.9.7.1 (from 2.7.15.7)
* cray-stat/4.11.9 (from 4.5.2)
* cray-tpsl,cray-tpsl-64/20.03.2 (from 19.06.1)
* cray-trilinos/12.18.1.1 (from 12.14.1.0)
* craype/2.7.10 (from 2.6.2)
* craype-dl-plugin-py3/21.02.1.3 (from 19.09.1)
* craypkg-gen/1.3.18 (from 1.3.7)
* gcc/11.2.0 (from 8.3.0)
* gdb4hpc/4.13.9 (from 3.0.10)   
* intel/19.1.2.254 (from 19.0.3.199)                   
* iobuf/2.0.10 (from 2.0.9)                    
* papi/6.0.0.12 (from 5.7.0.2)
* perftools-base/21.12.0 (from 7.1.1)
* pmi,pmi-lib/5.0.17 (from 5.0.14)
* valgrind4hpc/2.12.7 (from 1.0.0)

## Software versions to be removed

* atp/3.8.1
* cce/8.7.9, 10.0.1, 10.0.3
* cray-R/3.4.2, 3.6.3.1, 4.0.2.0
* cray-ccdb/3.0.4, 3.0.5, 4.8.1
* cray-cti/1.0.7, 1.0.10, 2.8.1
* cray-fftw/3.3.8.2, 3.3.8.6, 3.3.8.8
* cray-hdf5,cray-hdf5-parallel/1.10.2.0, 1.10.6.1, 1.12.0.0
* cray-lgdb/3.0.10
* cray-libsci/19.02.1, 20.06.1
* cray-mpich,cray-mpich-abi,cray-shmem/7.7.6, 7.7.14, 7.7.16
* cray-netcdf,cray-netcdf-hdf5parallel/4.6.1.3, 4.7.3.3, 4.7.4.0
* cray-parallel-netcdf/1.8.1.4, 1.12.0.1, 1.12.1.0
* cray-petsc,cray-petsc-64,cray-petsc-complex,cray-petsc-complex-64/3.9.3.0, 3.12.4.1, 3.13.3.0
* cray-python/2.7.15.6, 3.8.2.1, 3.8.5.0
* cray-stat/4.5.2, 4.7.2
* cray-tpsl,cray-tpsl-64/18.06.1
* cray-trilinos/12.12.1.1, 12.18.1.0
* craype/2.5.18, 2.6.5, 2.7.2
* craype-dl-plugin-py3/20.06.1, 20.10.1
* craypkg-gen/1.3.10, 1.3.11
* craype-ml-plugin-py2/1.0.1
* craype-ml-plugin-py3/1.0.1
* gcc/8.2.0, 9.3.0
* gdb4hpc/4.8.1
* iobuf/2.0.8
* java/jdk1.8.0_51
* modules/3.2.11.1
* papi/5.6.0.6, 6.0.0.1, 6.0.0.4
* perftools,perftools-base,perftools-lite/7.0.6
* perftools-base/20.06.0, 20.10.0
* pmi,pmi-lib/5.0.16
* valgrind4hpc/2.6.4, 2.8.1

## New software versions available

* atp/3.14.9
* cce/13.0.1
* cray-R/4.1.2.0
* cray-ccdb/4.12.8
* cray-cti/2.15.9
* cray-fftw/3.3.8.10
* cray-hdf5,cray-hdf5-parallel,cray-hdf5-parallel/1.12.1.1
* cray-mpich,cray-mpich-abi,cray-shmem/7.7.19
* cray-netcdf,cray-netcdf-hdf5parallel/4.8.1.1
* cray-parallel-netcdf/1.12.2.1
* cray-petsc,cray-petsc-64,cray-petsc-complex,cray-petsc-complex-64/3.14.5.0
* cray-python/3.9.7.1
* cray-stat/4.11.9
* craype/2.7.10
* craype-dl-plugin-py3/21.02.1.3
* craypkg-gen/1.3.18
* gcc/11.2.0
* gdb4hpc/4.13.9
* papi/6.0.0.12
* perftools-base/21.12.0
* valgrind4hpc/2.12.7
