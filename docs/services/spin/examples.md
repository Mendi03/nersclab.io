# Spin Examples

## Community Showcase

This section highlights approaches or techniques developed by Spin users.

If you'd like to share something you've done with Spin that you think
other users would find helpful, we'd love to add your work to this
showcase! Contact us by [submitting a ticket](https://help.nersc.gov/).

You can also find other Spin users in the #spin channel of the
[NERSC Users Slack workspace](https://www.nersc.gov/users/NUG/nersc-users-slack/).

### Managing Spin Apps with Helm

One challenge associated with running complex workloads in Kubernetes is
managing and tracking the complete state of the workload, with all of its
associated containers, services, and orchestration. [Helm](https://helm.sh) is
a tool designed to simplify the process of managing such complex workflows,
specifically using a feature called
[charts](https://helm.sh/docs/topics/charts/).

[This slide deck (PDF)](https://drive.google.com/file/d/1iy9bqS_GTtb2s5OzjnBRmpp0YnzbtJyZ/view)
describes NERSC user
[Valerie Hendrix](https://crd.lbl.gov/departments/data-science-and-technology/uss/staff/valerie-hendrix/)'s
process for implementing and using Helm charts to manage workflows in Spin.

[This video (MP4)](https://drive.google.com/file/d/10myW2FIwRkkD8bvwUwdfDgmefBjLsFVb/view)
shows Val demonstrating the approach for NERSC staff.

### Securing a website with LetsEncrypt

LetsEncrypt is a popular way to obtain a self-renewing web certificate to
secure a website. With LetsEncrypt, automated routines handle the tedious
validation, renewal, and installation of updated certificates. Plus, it's
free.

However, the standard LetsEncrypt process assumes that web certificates
are stored and accessed via a conventional file system, whereas in a
Kubernetes-based systems like Rancher, web certificates are stored as
secrets and referenced by ingress controllers.

[This slide deck (PDF)](https://drive.google.com/file/d/1N81QvkE5raeAas4Yx2DwVctK2uYQ_m5_/view)
describes NERSC user [Chris Harris](https://www.kitware.com/chris-harris/)'s
process for implementing LetsEncrypt in a Rancher environment.

[This video (MP4)](https://drive.google.com/file/d/16Iujbfaup4T-FYbYqOTzCNCOwlrx0iQy/view)
shows Chris demonstrating the approach for NERSC staff.
