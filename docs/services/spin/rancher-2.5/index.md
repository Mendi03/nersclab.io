# Rancher 2.5

This version of Rancher reached End-of-Life in October 5, 2022, and
was superseded by Rancher v2.6, which included a significant UI update.

**Rancher 2.5 is currently unavailable in the Spin platform and this 
documentation is provided only as a historical reference.**
